from datetime import datetime
import logging
import boto3
import ndjson
from precictforecasts import settings

class MinioUploader(object):
    s3_url = settings.S3_URL
    s3_bucket_name = settings.S3_BUCKET_NAME
    aws_access_key = settings.AWS_ACCESS_KEY
    aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)

    # Uploads array of dicts to ndjson file on minio
    def upload_array_to_ndjson_file(self, array_data, output_file_name_prefix):
        ndjson_data = ndjson.dumps(array_data)
        return self._upload_string_content(ndjson_data, output_file_name_prefix)

    def _upload_string_content(self, string_content, output_file_name_prefix):
        time_stamp = datetime.now().strftime("%Y-%m-%d_%I_%M_%S")
        file_name = f'{output_file_name_prefix}_{time_stamp}.jsonl'

        self.log.info(f'Writing output to file {file_name} in bucket {self.s3_bucket_name} on minio {self.s3_url}')

        session = boto3.Session(
            aws_access_key_id=self.aws_access_key,
            aws_secret_access_key=self.aws_secret_access_key,
        )

        s3 = session.resource(service_name='s3', endpoint_url=self.s3_url)

        s3.Object(self.s3_bucket_name, file_name).put(Body = string_content)
