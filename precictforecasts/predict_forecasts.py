import logging
import datetime
from precictforecasts.minio_downloader import MinioDownloader
from precictforecasts.minio_uploader import MinioUploader
from prediction_builder import create_predictions

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

def predict_forecasts():
    minio_downloader = MinioDownloader()
    minio_uploader = MinioUploader()

    for occupation in minio_downloader.download_occupation_timeseries():
        if occupation and isinstance(occupation, dict) and occupation.get('series') and isinstance(occupation.get('series'), list):
            # Dict for storing time serie on format, e.g:
            # {
            #     "1577836800000":1,
            #     "1577923200000":2
            #  }
            time_serie_dict = {}
            for occupation_serie in occupation['series']:
                if occupation_serie.get('date_timemillis') and occupation_serie.get('occurences'):
                    date_time = datetime.datetime.fromtimestamp(occupation_serie['date_timemillis'] / 1000)
                    time_serie_dict[str(date_time)] = int(occupation_serie['occurences'])

                # From format:
                # {"occupation": "f\u00f6rs\u00e4ljare", "series": [{"date_timemillis": 1482278400000, "occurences": 195}, {

                # Should be converted to:
                # {
                #     "1577836800000":1,
                #     "1577923200000":2,
                #     "1578009600000":4,

                # And is:
                # {'1451606400000': 199, '1451692800000': 117, '1451779200000': 74, '1451865600000': 582, '1451952000000': 422,
            predicted_forecasts = create_predictions(time_serie_dict)
            log.info('predicted forecast')

