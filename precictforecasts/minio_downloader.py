import json
import logging
import re
from datetime import datetime

import boto3
from botocore.client import Config

from precictforecasts import settings

class MinioDownloader(object):
    s3_url = settings.S3_URL
    s3_bucket_name = settings.S3_BUCKET_NAME
    aws_access_key = settings.AWS_ACCESS_KEY
    aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY
    occupations_src_file_prefix = settings.MERGE_TIMESERIES_OCCUPATIONS_SRC_FILE_PREFIX
    skills_src_file_prefix = settings.MERGE_TIMESERIES_SKILLS_SRC_FILE_PREFIX

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)

        self.s3 = boto3.resource('s3',
                                 endpoint_url=self.s3_url,
                                 aws_access_key_id=self.aws_access_key,
                                 aws_secret_access_key=self.aws_secret_access_key,
                                 config=Config(signature_version='s3v4'),
                                 region_name='us-east-1')

    # Downloads occupation timeseries for years from minio with specified file prefix...
    def download_occupation_timeseries(self):
        yield from self._download_items(self.occupations_src_file_prefix)

    # Downloads skills timeseries for years from minio with specified file prefix...
    def download_education_plans(self):
        yield from self._download_items(self.skills_src_file_prefix)

    def strip_timestamp_suffix_from_file(self, filename):
        stripped_filename = None
        if filename:
            filename_str = str(filename)
            iter = re.finditer('_[0-9]{4}-[0-9]{2}-[0-9]{2}.jsonl$', filename_str)
            index = None
            for match in iter:
                index = match.start()
                break
            stripped_filename = filename_str[:index]
        return stripped_filename


    # Downloads the most recent file (e.g. educations or education plans) that matches the specified prefix...
    def _download_items(self, file_prefix):
        my_bucket = self.s3.Bucket(self.s3_bucket_name)
        regex = '^' + file_prefix + '*'

        time_now = datetime.now()
        # Dict with keys and file paths to the files that should be parsed from:
        identified_files_to_download = {}

        for bucket_object in my_bucket.objects.all():
            file_name = str(bucket_object.key)
            self.log.debug(f'Current file: {file_name}')
            if re.match(regex, file_name):
                self.log.debug(f'{file_name} is matching prefix')
                self.log.debug(f'File size is {bucket_object.size}')


                if bucket_object.size > 0:
                    filename_excl_timestamp_suffix = self.strip_timestamp_suffix_from_file(file_name)

                    if filename_excl_timestamp_suffix:
                        # Put file path to identified_files_to_download if first occurrance...
                        if not identified_files_to_download.get(filename_excl_timestamp_suffix, None):
                            identified_files_to_download[filename_excl_timestamp_suffix] = {'file_name': file_name,
                                                                                   'last_modified': bucket_object.last_modified}
                        # Keep the most recently modified file if file path already exist in identified_files_to_download...
                        else:
                            self.log.debug(f'Already found a matching file. Check dates...')
                            identified_file = identified_files_to_download[filename_excl_timestamp_suffix]
                            if bucket_object.last_modified > identified_file.get('last_modified'):
                                identified_files_to_download[filename_excl_timestamp_suffix] = {'file_name': file_name,
                                                       'last_modified': bucket_object.last_modified}
        if identified_files_to_download:
                # Loop over identified_files_to_download and yield each row in files on minio as dict...
                for key, identified_file_to_download in identified_files_to_download.items():
                    file_to_download = identified_file_to_download.get('file_name')
                    line_counter = 0
                    self.log.info('Trying to read file %s from Minio' % file_to_download)
                    response = self.s3.Object(self.s3_bucket_name, file_to_download).get()
                    body = response['Body']

                    self.log.info('Streaming time_series from file: %s' % file_to_download)

                    for line in body.iter_lines():
                        try:
                            decoded_line = line.decode("utf-8")
                            line_counter += 1
                            if line_counter % 1000 == 0:
                                self.log.info('Downloaded %s lines from file: %s' % (line_counter, file_to_download))
                            yield json.loads(decoded_line)
                        except Exception as e:
                            self.log.error(f"Error when parsing line {e}")
                            yield None
        else:
            raise FileExistsError(
                'No file could be found according to the regex pattern: %s and a timestamp within %s hours' % (
                    regex, settings.MERGE_TIMESERIES_SRC_FILE_MAX_VALID_HOURS))

    # def _check_is_valid_timestamp(self, bucket_object, time_now):
    #     filelastmodified = bucket_object.last_modified
    #     # Remove timezone so the datetime can be compared to now_time.
    #     filelastmodified = filelastmodified.replace(tzinfo=None)
    #     difference = time_now - filelastmodified
    #     diff_in_hours = difference.total_seconds() / 3600
    #
    #     is_valid_timestamp = (diff_in_hours <= float(settings.MERGE_TIMESERIES_SRC_FILE_MAX_VALID_HOURS))
    #     if not is_valid_timestamp:
    #         self.log.debug('File %s is %s hours old right now and is not valid.' % (bucket_object.key, round(diff_in_hours, 2)))
    #
    #     return is_valid_timestamp
